## What's in it?

- Docker
- PHP 8
- MySQL
- Fresh laravel 8 at http://localhost
- Mailhog (catches email traffic)
- Redis
- Redis Manager at http://localhost:7000

## Run / Installation
- docker-compose up --build
    - This command will boot the project
- docker exec -it app bash
    - This command will bash you in the laravel framework directory (webroot)
- composer install
    - This command will install all the framework dependencies
- If no .env file created in the root, copy the .env.example and rename it to .env and run the following command: php artisan key:generate

### Useful commands
- docker exec -it app bash
- composer dump-autoload
- composer install
- php artisan make:migration create_some_table
- php artisan migrate
- php artisan migrate:fresh
